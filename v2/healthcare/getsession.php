<?php

// start the session so we can access session vars
if (session_id() == '') {
    session_start();
}


// using query string parameters
//$correlationId = (empty($_GET['correlationId'])) ? 'Not Specified'   : $_GET['correlationId'];
//$displayName = (empty($_GET['displayName'])) ? 'Not Specified' : $_GET['displayName'];
//$userName = (empty($_GET['userName'])) ? 'unknown' : $_GET['userName'];

// using json payload
$postdata = file_get_contents("php://input");
$request = json_decode($postdata);
$correlationId = $request->correlationId;
$displayName = $request->displayName;
$userName = $request->userName;

// configure the JSON to use in the session.
$json = '
    {
        "webAppId": "fusionweb1a4b25d47vdevesw4",
        "allowedOrigins": ["*"],
        "urlSchemeDetails": {
            "host": "172.84.0.121",
            "port": "8080",
            "secure": false
        },
        "voice":
        {
            "username": "%s",
            "displayName": "%s",
            "domain": "192.168.56.250",
            "inboundCallingEnabled": false
        },
        "uuiData": "%s",
        "additionalAttributes":
        {
            "AED2.metadata":
            {
                "role": "consumer"
            },
            "AED2.allowedTopic": "%s"
        }
    }
';

$json = sprintf($json, $userName, $displayName, $correlationId, $correlationId);

// configure the curl options
$ch = curl_init("http://172.84.0.121:8080/gateway/sessions/session");
curl_setopt($ch,CURLOPT_POST, true);
curl_setopt($ch,CURLOPT_POSTFIELDS, $json);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($json))
);

// execute HTTP POST & close the connection
$response = curl_exec($ch);
// add CORS header
header("Access-Control-Allow-Origin: *");

// decode the JSON and pick out the ID
$decodedJson = json_decode($response);
$id = $decodedJson->{'sessionid'};

// store the Web Gateway session ID in the HTTP session
$_SESSION['sessionid'] = $id;

echo $id;
echo "Hi";

curl_close($ch);

?>
