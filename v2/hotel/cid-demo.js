var config = assistConfig();

window.AssistSDK = {

    onConnectionEstablished : function() {
        console.log("on connection established called");
        
        localStorage.removeItem("cid-only");

        if (config.cobrowseOnly == true) {
            createCidGui();
            localStorage.setItem("cid-only", "cid-only");
        }
    },
    
    onInSupport : function() {  
        console.log("on in support called");
        
        if (localStorage.getItem("cid-only") == "cid-only") {
            createCidGui();
        }
    },
    
    onEndSupport : function() {  
        console.log("on end support called");
        
        if (localStorage.getItem("cid-only") == "cid-only") {
            var cidGui = document.getElementById("cid-gui");
            cidGui.parentNode.removeChild(cidGui);
        }
        
        localStorage.removeItem("cid-only");
    },
    onAnnotationAdded : function(annotation, sourceName) {
        if (QueryString.annotateCallback == 'true') {
            console.log("On annotation added called.");
            console.log("Source=" + sourceName);
            console.log("Stroke=" + annotation.stroke);
            console.log("Opacity=" + annotation.strokeOpacity);
            console.log("Width=" + annotation.strokeWidth);
            console.log("Path array length=" + annotation.points.length);
            var lastPoint = annotation.getLastPoint();
            if (lastPoint != null) {
             console.log("Last point (X,Y) is (" + annotation.getLastPoint().x + "," + annotation.getLastPoint().y + ")");
            }
        }
    },
    
    onAnnotationsCleared : function() {
        if (QueryString.annotateCallback == 'true') {
            console.log("On annotation cleared called");
        }
    },
    
    // Uncomment the following to provide co-browsing callbacks.
    
    //  onCobrowseActive : function() {
    //      console.log("Co-browsing is active.");
    //  },
    //  
    //  onCobrowseInactive : function() {
    //      console.log("Co-browsing is inactive.");
    //  }
    
};

function createCidGui() {
    var cidGui = document.createElement("div");
    cidGui.id = "cid-gui";
    cidGui.innerHTML = "Assist";
    
    var endButton = document.createElement("button");
    endButton.innerHTML = "End Support";
    
    endButton.addEventListener("click", function(event) {
        AssistSDK.endSupport();
    }, false);
    
    cidGui.appendChild(endButton);
    document.body.appendChild(cidGui);
    
}