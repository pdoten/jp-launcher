var config = assistConfig();


window.AssistSDK = {

    onConnectionEstablished : function() {
        console.log("on connection established called");
        
        localStorage.removeItem("cid-only");

        if (config.cobrowseOnly == true) {
            createCidGui();
            localStorage.setItem("cid-only", "cid-only");
        }
    },
    
    onInSupport : function() {  
        console.log("on in support called");
        
        if (localStorage.getItem("cid-only") == "cid-only") {
            createCidGui();
        }
    },
    
    onEndSupport : function() {  
        console.log("on end support called");
        
        if (localStorage.getItem("cid-only") == "cid-only") {
            var cidGui = document.getElementById("cid-gui");
            cidGui.parentNode.removeChild(cidGui);
        }
        
        localStorage.removeItem("cid-only");
    }
    
};

function createCidGui() {
    var cidGui = document.createElement("div");
    cidGui.id = "cid-gui";
    cidGui.innerHTML = "Assist";
    
    var endButton = document.createElement("button");
    endButton.innerHTML = "End Support";
    
    endButton.addEventListener("click", function(event) {
        AssistSDK.endSupport();
    }, false);
    
    cidGui.appendChild(endButton);
    document.body.appendChild(cidGui);
}