AppsArea - Bootstrap Hotel

	* Bootstrap Hotel
	* Version: 1.0 from 19th January 2013
	* Author: appman

DESCRIPTION

**Bootstrap Hotel is a professional hotel template designed entirely using Bootstrap. It's designed for fast and easy booking after studying numerous different hotel websites. The most attractive aspect of hotel websites is large images and minimal text and this theme exemplifies that. What's more is that it loads fast!** 

Most other hotel websites have an external booking engine added to the site that looks totally different. This theme provides 3 different versions of the booking form to make it available on almost all pages. This convenience allows a smoother booking experience for the user. Bootstrap Hotel is all about showing the user the best features of the hotel. It also contains some niceties such as the ability to view the room while booking.

Bootstrap Hotel makes use of a customized jquery calendar and a JavaScript booking form. Steps leading to the payment page is also included. If you want to make your own hotel website, this is the theme to use.

It's also compatible with any of the themes from http://bootswatch.com/ and we've included the LESS files - making it very easy to change. You can also easily add and remove components as it's based on the bootstrap framework.

Pages
=============

Home page with booking form
Stand alone booking page for selecting rooms and dates
Reservation page
Payment page
Promotions
Gallery
Facilities and services - 2 column
View rooms - 3 column
Custom Google map of the hotel
A general page with the booking form on the sidebar for any other pages you may want to add
A typography page so you can easily use the premade styles

Code
=============

* Compatible with Bootstrap 2.2.x
* Uses Bootstrap 2.2.2
* Written in valid HTML5 that makes good use of proper semantics
* Theme modifications are stored in a separate CSS stylesheet to make upgrading easy
* View the layout without styles to see the well-formed, bare markup
* Includes jQuery v1.8.3, jQuery UI v1.9.2, Quicksand 1.2.2, SocialCount - v0.1.4, jQuery Nivo Slider v3.1

Tested browsers
=============

* Firefox 18+
* Latest Chrome
* Latest Safari
* Internet Explorer 9+
* Opera 11

Assets
=============

* Bootstrap Twitter - http://twitter.github.com/bootstrap/
* Bootswatch - http://bootswatch.com/
* jQuery - http://jquery.com/

Troubleshooting
=================

If you have any problems please contact us at admin@AppsArea.com.

Note
=============

Hotel images taken from pixabay.com, flickr.com and promotional content

	Carousel
	http://wallpapersget.com/tag/maldives/
	http://www.my-walls.net/nature-summer-season-maldives-hotel/
	http://1stepwebdesign.com/freebies/2394/

	Home (lower 4)
	http://www.privateislandsmag.com/2011/11/viceroy-maldives-resort-the-door-opens-for-villa-ownership/
	http://www.publicdomainpictures.net/view-image.php?image=18212&picture=floating-boats
	http://www.expedia.ca/Maldives-Hotels-Velassaru-Maldives.h1145793.Hotel-Information
	http://www.expedia.co.uk/Hanoi-Hotels-Hilton-Garden-Inn-Hanoi.h5477538.Hotel-Information

	Rooms
	http://cls.cdn-hotels.com/hotels/1000000/550000/546100/546039/546039_75_b.jpg
	http://www.booking.com/hotel/gr/kassandra-bay.en.html
	http://www.booking.com/hotel/gr/rodos-park-suites.nl.html
	http://www.visitmaldives.com/en/resorts/lily-beach-resort-spa
	http://luxuryworldwidecollection.com/resort.asp?pid=169
	http://piximus.net/others/kanuhura-maldives
	http://www.hotels.com/ho212456/vivanta-by-taj-coral-reef-maldives-maldives-all-maldives/
	http://www.hotels.com/ho402269/saigon-sports-3-hotel-ho-chi-minh-city-vietnam/

	Facilities
	http://www.asiatophotel.com/maldives/maldives/shangri_la_s_villingili_resort_spa/
	http://www.finestlodges.com/en/hotel/Gramercy%20Park%20Hotel-1370
	http://www.hotels.com/ho350294/natal-ocean-club-maxaranguape-brazil/
	http://www.kiwicollection.com/hotel-detail/four-seasons-resort-maldives-at-kuda-huraa
	http://www.karmakerala.com/news/2010/06/15/commercial-deep-water-diving-a-new-job-option-in-kerala/
	http://www.hotels.com/ho224981/sheraton-maldives-full-moon-resort-spa-maldives-all-maldives/

	Gallery
	http://www.flickr.com/photos/sackerman519/5047574943/
	http://www.flickr.com/photos/nattu/3217481781/
	http://www.publicdomainpictures.net/view-image.php?image=2872&picture=diver-feeding-fish
	http://www.publicdomainpictures.net/view-image.php?image=12451&picture=fish-underwater
	http://www.flickr.com/photos/mal-b/6835339556/
	http://www.flickr.com/photos/spritey/5215547933/
	http://www.flickr.com/photos/hotelcasavelas/4163539098/
	http://www.publicdomainpictures.net/view-image.php?image=9769&picture=healthy-breakfast
	http://www.booking.com/hotel/id/jeda-villa.en.html
	http://www.flickr.com/photos/hotelcasavelas/4173736174/
	http://www.flickr.com/photos/sackerman519/5086986042/
	http://www.flickr.com/photos/bruchez/5516013544/
	http://www.flickr.com/photos/worldwide-souvenirs/7543504638/
	http://www.flickr.com/photos/cs-jay/1774699799/
	http://www.flickr.com/photos/kashmut/4303391245/sizes/z/
	http://www.flickr.com/photos/12394349@N06/7955308640/sizes/l/
	http://www.flickr.com/photos/warrenski/6500484493/sizes/l/
	http://www.publicdomainpictures.net/view-image.php?image=21648&picture=restaurant-tables-with-sea-view
	http://www.publicdomainpictures.net/view-image.php?image=4975&picture=jacuzzi
	http://www.publicdomainpictures.net/view-image.php?picture=palms-and-sea&image=4519&large=1&jazyk=CS
	http://www.flickr.com/photos/sackerman519/5048040027/
	http://www.flickr.com/photos/kashmut/4303391245/