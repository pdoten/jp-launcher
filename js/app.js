  // cache the UI elements that we will be interacting with
    var $endButton = $('#end');
    var $codeDisplay = $('#short-code');
    

    UC.onInitialised = function() {

    //create topic

    var topic = UC.aed.createTopic("queue");


        // Full Live Assist

        $("#help").on("click", function() {

            //send data to queue topic

            topic.submitData("call", "assist : "+ window.location.href );

            topic.onSubmitDataSuccess = function(key, value, version) {

                //subscribe to queue topic
                topic.connect();

                //Ontopic update, if agent then startSupport
                topic.onTopicUpdate = function(key, data, version, deleted) {
                    if (key == "agent") {
                        AssistSDK.startSupport({destination: data});
                    }
                }
            }

        });

           // Agent Video Only Live Assist

        $("#agent").on("click", function() {

            //send data to queue topic

            topic.submitData("call", "agent only : "+ window.location.href );

            topic.onSubmitDataSuccess = function(key, value, version) {

                //subscribe to queue topic
                topic.connect();

                //Ontopic update, if agent then startSupport
                topic.onTopicUpdate = function(key, data, version, deleted) {
                    if (key == "agent") {
                        AssistSDK.startSupport({destination: data, videoMode : "agentOnly"});
                    }
                }
            }

        });


        // Audio only Assist

        $("#audio").on("click", function() {

            //send data to queue topic

            topic.submitData("call", "audio : "+ window.location.href );

            topic.onSubmitDataSuccess = function(key, value, version) {

                //subscribe to queue topic
                topic.connect();

                //Ontopic update, if agent then startSupport
                topic.onTopicUpdate = function(key, data, version, deleted) {
                    if (key == "agent") {
                        AssistSDK.startSupport({destination: data, videoMode : "none" });
                    }
                }
            }

        });

    };


   $.post('../../session.php', function (sessionToken) {
        
        UC.start(sessionToken);

    })
    .fail(function (response) {
        alert('Failed to get session :-(');
    });




    //short code
    $('#shortcode').click(function (event) {
        event.preventDefault();
        
        // Retrieve a short code
        $.ajax('../../shortcode.php', {
            dataType: 'json'
        })
        .done(function (response) {

            // display the code for the consumer to read to the agent
            // $codeDisplay.text(response.shortCode);

            alert("Tell your agent this short code: "+response.shortCode);

            var appkey = response.shortCode;

            // Retrieve session configuration (session token, CID etc.)
            $.get('../../shortcode-session.php?appkey='+appkey, function (config) {

                // translate the config retrieved from the server, into
                // parameters expected by AssistSDK
                var params = {
                    cobrowseOnly: true,
                    correlationId: config.cid,
                    scaleFactor: config.scaleFactor,
                    sessionToken: config['session-token']
                };

                // start the support session
                AssistSDK.startSupport(params);

                // configure Assist to automatically approve share requests
                AssistSDK.onScreenshareRequest = function () {
                    return true;
                };

                

            }, 'json');
             
        });
    });


    // the $endSupport button will be hidden
    // $endButton.hide().css('display', 'inline');

    // when screenshare is active, toggle the visibility of the 'end' button
    AssistSDK.onInSupport = function () {
        $endButton.show();
    };

    AssistSDK.onEndSupport = function () {
        $endButton.hide();
        $codeDisplay.text('');
    };

    $endButton.click(function () {
        AssistSDK.endSupport();
    });